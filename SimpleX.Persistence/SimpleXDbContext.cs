﻿using Microsoft.EntityFrameworkCore;
using SimpleX.Domain.Entities;
using SimpleX.Persistence.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Persistence
{
    public class SimpleXDbContext : DbContext
    {
        public SimpleXDbContext(DbContextOptions<SimpleXDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Droplet> Droplets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyAllConfigurations();
        }
    }
}
