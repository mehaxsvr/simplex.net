﻿using SimpleX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Persistence
{
    public class SimpleXInitializer
    {
        private readonly Dictionary<int, User> Users = new Dictionary<int, User>();
        private readonly Dictionary<int, Droplet> Droplets = new Dictionary<int, Droplet>();

        public static void Initialize(SimpleXDbContext context)
        {
            var initializer = new SimpleXInitializer();
            initializer.SeedEverything(context);
        }

        public void SeedEverything(SimpleXDbContext context)
        {
            context.Database.EnsureCreated();

            
        }
    }
}
