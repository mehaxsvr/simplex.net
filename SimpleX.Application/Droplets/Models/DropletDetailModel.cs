﻿using SimpleX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SimpleX.Application.Droplets.Models
{
    public class DropletDetailModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? UserId { get; set; }
        public int? DoId { get; set; }
        public int? Vcpus { get; set; }
        public double? Memory { get; set; }
        public string Host { get; set; }
        public int? SshPort { get; set; }
        public string SshUser { get; set; }
        public string SshPass { get; set; }

        public static Expression<Func<Droplet, DropletDetailModel>> Projection
        {
            get
            {
                return droplet => new DropletDetailModel
                {
                    Id = droplet.DropletId,
                    Name = droplet.Name,
                    UserId = droplet.UserId,
                    DoId = droplet.DoId,
                    Vcpus = droplet.Vcpus,
                    Memory = droplet.Memory,
                    Host = droplet.Host,
                    SshPort = droplet.SshPort,
                    SshUser = droplet.SshUser,
                    SshPass = droplet.SshPass
                };
            }
        }

        public static DropletDetailModel Create(Droplet droplet)
        {
            return Projection.Compile().Invoke(droplet);
        }
    }
}
