﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Application.Droplets.Models
{
    public class DropletListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
