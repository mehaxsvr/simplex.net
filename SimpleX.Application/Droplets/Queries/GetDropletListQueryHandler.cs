﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SimpleX.Application.Droplets.Models;
using SimpleX.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleX.Application.Droplets.Queries
{
    public class GetDropletListQueryHandler : IRequestHandler<GetDropletListQuery, List<DropletListModel>>
    {
        private readonly SimpleXDbContext _context;

        public GetDropletListQueryHandler(SimpleXDbContext context)
        {
            _context = context;
        }

        public Task<List<DropletListModel>> Handle(GetDropletListQuery request, CancellationToken cancellationToken)
        {
            return _context.Droplets.Select(c => new DropletListModel
            {
                Id = c.DropletId,
                Name = c.Name
            }).ToListAsync(cancellationToken);
        }
    }
}
