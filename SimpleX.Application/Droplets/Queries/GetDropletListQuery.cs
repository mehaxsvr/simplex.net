﻿using MediatR;
using SimpleX.Application.Droplets.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Application.Droplets.Queries
{
    public class GetDropletListQuery : IRequest<List<DropletListModel>>
    {
        public string Id { get; set; }
    }
}
