﻿using MediatR;
using SimpleX.Application.Droplets.Models;
using SimpleX.Application.Exceptions;
using SimpleX.Domain.Entities;
using SimpleX.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleX.Application.Droplets.Queries
{
    public class GetDropletDetailQueryHandler : IRequestHandler<GetDropletDetailQuery, DropletDetailModel>
    {
        private readonly SimpleXDbContext _context;

        public GetDropletDetailQueryHandler(SimpleXDbContext context)
        {
            _context = context;
        }

        public async Task<DropletDetailModel> Handle(GetDropletDetailQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Droplets.FindAsync(request.Id);

            if (entity == null)
                throw new NotFoundException(nameof(Droplet), request.Id);

            return new DropletDetailModel
            {
                Id = entity.DropletId,
                Name = entity.Name,
                UserId = entity.UserId,
                DoId = entity.DoId,
                Vcpus = entity.Vcpus,
                Memory = entity.Memory,
                Host = entity.Host,
                SshPort = entity.SshPort,
                SshUser = entity.SshUser,
                SshPass = entity.SshPass
            };
        }
    }
}
