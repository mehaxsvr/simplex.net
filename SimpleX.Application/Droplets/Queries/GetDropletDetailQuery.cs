﻿using MediatR;
using SimpleX.Application.Droplets.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Application.Droplets.Queries
{
    public class GetDropletDetailQuery : IRequest<DropletDetailModel>
    {
        public int Id { get; set; }
    }
}
