﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Domain.Enumerations
{
    public enum UserLevel
    {
        User = 0,
        Admin = 1
    }
}
