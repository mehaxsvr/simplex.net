﻿using SimpleX.Domain.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Domain.Entities
{
    public class User
    {
        public User()
        {
            Droplets = new HashSet<Droplet>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public UserLevel Level { get; set; }
        public string Email { get; set; }
        public string DigitalOceanToken { get; set; }

        public ICollection<Droplet> Droplets { get; set; }
    }
}
