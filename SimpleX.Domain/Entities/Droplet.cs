﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleX.Domain.Entities
{
    public class Droplet
    {
        public Droplet()
        {

        }

        public int DropletId { get; set; }
        public string Name { get; set; }
        public int? UserId { get; set; }
        public int? DoId { get; set; }
        public int? Vcpus { get; set; }
        public double? Memory { get; set; }
        public string Host { get; set; }
        public int? SshPort { get; set; }
        public string SshUser { get; set; }
        public string SshPass { get; set; }

        public User User;
    }
}
