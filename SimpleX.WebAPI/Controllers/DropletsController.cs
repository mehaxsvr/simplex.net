﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleX.Application.Droplets.Models;
using SimpleX.Application.Droplets.Queries;
using SimpleX.WebAPI.Infrastructure;

namespace SimpleX.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DropletsController : BaseController
    {
        [HttpGet]
        public Task<List<DropletListModel>> Get()
        {
            return Mediator.Send(new GetDropletListQuery());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetDropletDetailQuery { Id = id }));
        }
    }
}